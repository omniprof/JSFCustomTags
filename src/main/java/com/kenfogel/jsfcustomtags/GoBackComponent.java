/*
 * Taken from http://www.mastertheboss.com/javaee/jsf/jsf-java-based-custom-tags-example
 */
package com.kenfogel.jsfcustomtags;

import java.io.IOException;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This example demonstrates how a Java class can return anything when called by
 * a tag. This should give you some insight into how PrimeFaces works.
 *
 */
@FacesComponent(value = "goback")
public class GoBackComponent extends UIComponentBase {

    private final static Logger LOG = LoggerFactory.getLogger(GoBackComponent.class);

    private final String back;
    private final String back1;
    private final String back2;

    @Override
    public String getFamily() {
        return "goBackComponent";
    }

    /**
     * Constructor initializes strings
     */
    public GoBackComponent() {
        back = "<input type=\"button\" value=\"Go Back From Whence You Came!\" onclick=\"history.back(-1)\" />";
        back1 = "<input type=\"button\" value=\"";
        back2 = "\" onclick=\"history.back(-1)\" />";
    }

    /**
     * This method is called by the tag
     *
     * @param context
     * @throws IOException
     */
    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        String value = (String) getAttributes().get("value");

        ResponseWriter writer = context.getResponseWriter();
        if (value == null) {
            writer.write(back);
        } else {
            writer.write(back1 + value + back2);
        }
    }
}
