/*
 * Taken from http://www.mastertheboss.com/javaee/jsf/jsf-java-based-custom-tags-example
 */
package com.kenfogel.jsfcustomtags;

import java.io.IOException;
import java.util.HashMap;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FacesComponent(value = "dictionary")
public class TranslatorComponent extends UIComponentBase {

    private final static Logger LOG = LoggerFactory.getLogger(TranslatorComponent.class);

    HashMap<String, String> dictionary = new HashMap<>();

    @Override
    public String getFamily() {
        return "translatorComponent";
    }

    public TranslatorComponent() {
        dictionary.put("dog", "ogday");
        dictionary.put("cat", "atcay");
        dictionary.put("mouse", "ousemay");
    }

    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        String value = (String) getAttributes().get("value");

        // If there is no value then do not do nothing
        if (value != null) {

            ResponseWriter writer = context.getResponseWriter();
            String translation = dictionary.get(value);
            if (translation == null) {
                writer.write("Sorry word not found!");
            } else {
                writer.write(translation);
            }
        }
    }
}
